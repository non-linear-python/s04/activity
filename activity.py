artist = {
	"artist_name" : "Saga",
	"Age" : 44,
	"company" : "BigHit Entertaintment"
}
band={
	"band_name" : "BTS",
	"years_active" : 10,
	"hit_songs" : ['Dynamite", "Mic Drop', "Fire"],
	"is_active":True
}
artist.update(band)
print(artist)
print('')

student_info = {
	"class" : {
		"student" : {
			"name": "Joon",
			"marks" : {
				"physics" : 85,
				"history" :90
			}
		}
	}
}
print("History score: "+str(student_info["class"]["student"]["marks"]["history"]))
#print(f"History grade: {student_info["class"]["student"]["marks"]["history"]}")
print('')

personal_info = {
	"name": "Jean",
	"age": 31,
	"salary" : 45000,
	"city": "Seoul"
} 
#Keys to extract
keys = ["name", "salary"]
info=dict(
	{
		"name":personal_info["name"],
		"salary":personal_info["salary"]
	}
)
print(info)	
print('')

employees = {
	"empl" : {"full_name": "Any Santiago", "salary": "45000"},
	"emp2":{"full name" : "Charles Boyle", "salary": "50000"},
	"emp3": {"full_name":"Rosa Diaz", "salary": "40000"},
	"emp4":{"full_name":"Jake Peralta", "salary": "45000"}
}
employees["emp4"]["salary"] = 55000
print(employees)
print('')

exam_data = {
	'name': ['Dwight', 'Michael', 'Jim', 'Pam', "Andy"],
	'score': [12.5, 5, 10, 16.5, 91],
	"attempts": [1, 3, 2, 1, 3],
	"qualify": ['yes', 'no', 'yes', 'yes', 'no']
}
import pandas as pd
df = pd.DataFrame(exam_data, index=["a", "b", "c", "d", "e"])
print(df)
print('')
print(df.iloc[2])